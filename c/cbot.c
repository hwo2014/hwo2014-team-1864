#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <assert.h>

#include "cJSON.h"
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    } else {
		//log all puts("...");
	}
}

struct Piece {
	int index;
	double inPieceDistance;
	double pradius, pangle, plength, pswitch;
	struct Piece * next;
}; 
struct Piece Pieces;

int main(int argc, char *argv[])
{
    int sock;
    cJSON *json;

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);

    json = join_msg(argv[3], argv[4]);
    write_msg(sock, json);
    cJSON_Delete(json);

	int start = 1, i = 0;
	double prevAngle = 0.0;
	int track_piece_index = -1, current_piece_index = -1;
	
    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type, *data;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        data = cJSON_GetObjectItem(json, "data");
        
        if (msg_type == NULL)
            error("missing msgType field");

		static char * myCar_color = NULL;
		double throttle = 1.0;
		
        msg_type_name = msg_type->valuestring;
        
        if (!strcmp("yourCar", msg_type_name)) {
				myCar_color = cJSON_GetObjectItem(data, "color")->valuestring;
				assert(!!myCar_color);
				printf("my_car_color read:%s, len:%i\n", myCar_color, strlen(myCar_color));
		} else if (start && !strcmp("carPositions", msg_type_name)) {
				msg = throttle_msg(1.0);
				start = 0;
        } else if (!strcmp("gameInit", msg_type_name)) {
			cJSON * race = cJSON_GetObjectItem(data, "race");
			cJSON * track = cJSON_GetObjectItem(race, "track");
			cJSON * pieces = cJSON_GetObjectItem(track, "pieces");			
			for (i = 0 ; i < cJSON_GetArraySize(pieces) ; i++) {
				cJSON * piece = cJSON_GetArrayItem(pieces, i);
				Pieces.pangle = piece->valuedouble;
				
				double piece_angle = piece->valuedouble;
				double piece_length = piece->valuedouble;
				int piece_switch = piece->valueint;
				double piece_radius = piece->valuedouble;
				printf("piece index: %d\n", ++track_piece_index);
				if (piece_angle) {					
					printf("piece_angle:%f\n",piece_angle);
					printf("piece_radius:%f\n",piece_radius);
				} else if (piece_length) {
					printf("piece_length:%f\n",piece_length);
				}
				//TODO game init
			}			
			log_message(msg_type_name, json);
		} else if (!strcmp("carPositions", msg_type_name)) {
			for (i = 0 ; i < cJSON_GetArraySize(data) ; i++) {
				cJSON * car = cJSON_GetArrayItem(data, i);
				cJSON * car_id = cJSON_GetObjectItem(car, "id");

				const char * car_color = strdup(cJSON_GetObjectItem(car_id, "color")->valuestring);

				cJSON * piece_position = cJSON_GetObjectItem(car, "piecePosition");
				cJSON * piece_index = cJSON_GetObjectItem(piece_position, "pieceIndex");
				cJSON * inPieceDist = cJSON_GetObjectItem(piece_position, "inPieceDistance");
				current_piece_index = piece_index->valueint;
				
				//printf("car_color read:%s len: %d\n", car_color, strlen(car_color));
				
				int mycar = !strcmp(myCar_color, car_color);

				if (mycar) {					
//				if (!strcmp("red\0", car_color)) {
					double angle = cJSON_GetObjectItem(car, "angle")->valuedouble;
					#define ANGLE_MAX 25.0
					double norm_angle = MAX(MIN(fabs(angle)/ ANGLE_MAX,1.0),0.0);
					double angleDelta = angle - prevAngle;
					if (norm_angle >= 0.3) {
						throttle = 1.0 - norm_angle * 3.0;
					} else if (norm_angle >= 0.2) {
						throttle = 1.0 - norm_angle * 2.0;
					} else  if(norm_angle < 0.05) { // break before next curve
						//break_beofore_curve();
						//take_outer_lane();
						throttle = 1.0 - norm_angle;						
					} else { //pedal to the medal if all fails
						throttle = 1.0;
					}
					
					printf("%d %f %f\n", current_piece_index, throttle, norm_angle);
				}				
				msg = throttle_msg(MAX(MIN(throttle / ANGLE_MAX,1.0),0.0));
			}
        } else {
            log_message(msg_type_name, json);
            msg = ping_msg();
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        //cJSON_Delete(json);
    }

    return 0;
}

/*cJSON *item = cJSON_GetObjectItem(items,"items");
  for (i = 0 ; i < cJSON_GetArraySize(item) ; i++)
  {
     cJSON * subitem = cJSON_GetArrayItem(item, i);
     name = cJSON_GetObjectItem(subitem, "name");
  }*/



static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
